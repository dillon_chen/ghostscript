%global _hardened_build 1
# override the default location of documentation or license files
# in 'ghostscript' instead of in 'libgs'
%global _docdir_fmt     %{name}
# download version
%global version_short   %(echo "%{version}" | tr -d '.')
# Obtain the location of Google Droid fonts directory
%global google_droid_fontpath %%(dirname $(fc-list : file | grep "DroidSansFallback"))

Name:             ghostscript
Version:          9.56.1
Release:          9
Summary:          An interpreter for PostScript and PDF files
License:          AGPL-3.0-or-later
URL:              https://ghostscript.com/
Source0:          https://github.com/ArtifexSoftware/ghostpdl-downloads/releases/download/gs%{version_short}/ghostscript-%{version}.tar.xz

Patch0:  ghostscript-9.23-100-run-dvipdf-securely.patch

# https://git.ghostscript.com/?p=ghostpdl.git;a=commitdiff;h=37ed5022cecd
# https://bugs.ghostscript.com/show_bug.cgi?id=706494
# fix CVE-2023-28879 Buffer Overflow in s_xBCPE_process
Patch101:  CVE-2023-28879.patch

# https://git.ghostscript.com/?p=ghostpdl.git;a=commitdiff;h=505eab7782b429017eb434b2b95120855f2b0e3c and
# https://git.ghostscript.com/?p=ghostpdl.git;a=commitdiff;h=0974e4f2ac0005d3731e0b5c13ebc7e965540f4d
# fix see https://bugs.ghostscript.com/show_bug.cgi?id=706761
# "OS command injection in %pipe% access"
# and https://bugs.ghostscript.com/show_bug.cgi?id=706778
# "%pipe% allowed_path bypass"
Patch102:  CVE-2023-36664.patch

# https://git.ghostscript.com/?p=ghostpdl.git;a=commitdiff;h=d81b82c70bc1
# CVE-2023-38559.patch fix
# "out of bounds read devn_pcx_write_rle() could result in DoS"
# and https://bugs.ghostscript.com/show_bug.cgi?id=706897
# which is in base/gdevdevn.c the same issue
# "ordering in if expression to avoid out-of-bounds access"
# as the already fixed CVE-2020-16305 in devices/gdevpcx.c
# see https://bugs.ghostscript.com/show_bug.cgi?id=701819
Patch103:  CVE-2023-38559.patch
Patch104:  backport-CVE-2023-46751.patch
Patch105:  fix-cve-2023-52722.patch
Patch106:  fix-CVE-2024-29510.patch
Patch107:  fix-CVE-2024-33869.patch
Patch108:  fix-CVE-2024-33870.patch
Patch109:  fix-CVE-2024-33871.patch
# https://bugs.ghostscript.com/show_bug.cgi?id=707510
# CVE-2024-29506 CVE-2024-29507 CVE-2024-29508 CVE-2024-29509 CVE-2024-29511
# CVE-2024-29509
Patch110:  Bug-707510-don-t-use-strlen-on-passwords.patch
# CVE-2024-29506
Patch111:  Bug-707510-don-t-allow-PDF-files-with-bad-Filters-to.patch
# CVE-2024-29507
Patch112:  Bug-707510-3-Bounds-checks-when-using-CIDFont-relate.patch
# CVE-2024-29508
Patch113:  Bug-707510-review-printing-of-pointers.patch
# CVE-2024-29511
Patch114:  Bug-707510-5-Reject-OCRLanguage-changes-after-SAFER-.patch
Patch115:  Bug-707510-5-2-The-original-fix-was-overly-aggressive.patch

Patch116:  Bug-707510-fix-LIBIDN-usage.patch

# See bug thread for details
#This is the second part of the fix for CVE-2024-33869
Patch117:  fix-CVE-2024-33869-second.patch

BuildRequires:    automake gcc
BuildRequires:    adobe-mappings-cmap-devel adobe-mappings-pdf-devel
BuildRequires:    google-droid-sans-fonts urw-base35-fonts-devel
BuildRequires:    cups-devel dbus-devel fontconfig-devel
BuildRequires:    lcms2-devel libidn-devel libijs-devel libjpeg-turbo-devel
BuildRequires:    libpng-devel libpaper-devel libtiff-devel openjpeg2-devel
BuildRequires:    zlib-devel gtk3-devel libXt-devel
BuildRequires:    jbig2dec-devel >= 0.16

Requires:         adobe-mappings-cmap
Requires:         adobe-mappings-cmap-lang
Requires:         adobe-mappings-pdf
Requires:         google-droid-sans-fonts
Requires:         urw-base35-fonts

Obsoletes:      %{name}-doc < %{version}-%{release}
Obsoletes:      %{name}-x11 < %{version}-%{release}
Obsoletes:      %{name}-gtk < %{version}-%{release}
Obsoletes:      %{name}-tools-printing < %{version}-%{release}
Obsoletes:      %{name}-tools-fonts < %{version}-%{release}
Obsoletes:      libgs < %{version}-%{release}
Provides:       %{name}-doc
Provides:       %{name}-x11
Provides:       %{name}-gtk
Provides:       %{name}-tools-printing
Provides:       %{name}-tools-fonts
Provides:       libgs
Provides:       %{name}-core

%description
Ghostscript is an interpreter for PostScript™ and Portable Document Format (PDF) files.
Ghostscript consists of a PostScript interpreter layer, and a graphics library.

%package devel
Summary:          Development files for Ghostscript's library
Requires:         %{name} = %{version}-%{release}

Obsoletes:        libgs-devel < %{version}-%{release}
Provides:         libgs-devel

%description devel
This package contains development files for %{name}.

%package        help
Summary:        Documents for %{name}
Buildarch:      noarch
Requires:       man info
Requires:       %{name} = %{version}-%{release}

Obsoletes:      %{name}-doc < %{version}-%{release}
Provides:       %{name}-doc

%description help
Man pages and other related documents for %{name}.

%package tools-dvipdf
Summary:          Ghostscript's 'dvipdf' utility
Requires:         %{name} = %{version}-%{release}
Requires:         texlive-dvips

%description tools-dvipdf
This package provides the utility 'dvipdf' for converting of TeX DVI files into
PDF files using Ghostscript and dvips

%prep
%setup -q -n %{name}-%{version}
%patch -P0 -p1
%patch -P101 -p0
%patch -P102 -p0
%patch -P103 -p0
%patch -P104 -p1
%patch -P105 -p1
%patch -P106 -p1
%patch -P107 -p1
%patch -P108 -p1
%patch -P109 -p1
%patch -P110 -p1
%patch -P111 -p1
%patch -P112 -p1
%patch -P113 -p1
%patch -P114 -p1
%patch -P115 -p1
%patch -P116 -p1
%patch -P117 -p1


# Libraries that we already have packaged(see Build Requirements):
rm -rf cups/libs freetype ijs jbig2dec jpeg lcms2* libpng openjpeg tiff zlib
rm -rf windows

%build
%configure --enable-dynamic --disable-compile-inits --without-versioned-path \
           --with-fontpath="%{urw_base35_fontpath}:%{google_droid_fontpath}:%{_datadir}/%{name}/conf.d/"
%make_build so

%install
# to install necessary files without 'make_install'
make DESTDIR=%{buildroot} soinstall

# rename to 'gs' binary.
mv -f %{buildroot}%{_bindir}/{gsc,gs}

# remove files
rm -f %{buildroot}%{_bindir}/{lprsetup.sh,unix-lpr.sh}
rm -f %{buildroot}%{_docdir}/%{name}/{AUTHORS,COPYING,*.tex,*.hlp,*.txt}
rm -f %{buildroot}%{_datadir}/%{name}/doc

# move some files into html/
install -m 0755 -d %{buildroot}%{_docdir}/%{name}/html
cp doc/gsdoc.el %{buildroot}%{_docdir}/%{name}/
mv -f %{buildroot}%{_docdir}/%{name}/{*.htm*,*.el,html}

# create symlink
ln -s %{_bindir}/gs %{buildroot}%{_bindir}/ghostscript
ln -s %{_mandir}/man1/gs.1 %{buildroot}%{_mandir}/man1/ghostscript.1

# use the symlinks where possible.
ln -fs %{google_droid_fontpath}/DroidSansFallback.ttf %{buildroot}%{_datadir}/%{name}/Resource/CIDFSubst/DroidSansFallback.ttf

for font in $(basename --multiple %{buildroot}%{_datadir}/%{name}/Resource/Font/*); do
  ln -fs %{urw_base35_fontpath}/${font}.t1 %{buildroot}%{_datadir}/%{name}/Resource/Font/${font}
done

# create symlink for each of the CMap files in Ghostscript's Resources/CMap folder.
for file in $(basename --multiple %{buildroot}%{_datadir}/%{name}/Resource/CMap/*); do
  find %{adobe_mappings_rootpath} -type f -name ${file} -exec ln -fs {} %{buildroot}%{_datadir}/%{name}/Resource/CMap/${file} \;
done

install -m 0755 -d %{buildroot}%{_datadir}/%{name}/conf.d/

%check
%make_build check

%files
%license LICENSE doc/COPYING
%{_datadir}/%{name}/
%dir %{_datadir}/%{name}/conf.d/
%{_bindir}/gs
%{_bindir}/gsnd
%{_bindir}/ghostscript
%{_bindir}/eps2*
%{_bindir}/pdf2*
%{_bindir}/ps2*
%{_bindir}/gsx
%{_bindir}/gsbj
%{_bindir}/gsdj
%{_bindir}/gsdj500
%{_bindir}/gslj
%{_bindir}/gslp
%{_bindir}/pphs
%{_bindir}/pf2afm
%{_bindir}/pfbtopfa
%{_bindir}/printafm
%{_libdir}/libgs.so.*
%{_libdir}/%{name}/

%files devel
%{_libdir}/libgs.so
%{_includedir}/%{name}/

%files help
%{_mandir}/man1/*
%lang(de) %{_mandir}/de/man1/*
%doc %{_docdir}/%{name}/

%files tools-dvipdf
%{_bindir}/dvipdf

%changelog
* Thu Aug 22 2024 Funda Wang <fundawang@yeah.net> - 9.56.1-9
- rebuild for new libpaper

* Fri Jul 12 2024 zhangxianting <zhangxianting@uniontech.com> - 9.56.1-8
- Type:CVE
- ID:NA
- SUG:NA
- DECS: This is the second part of the fix for CVE-2024-29511

* Fri Jul 12 2024 zhangxingrong-<zhangxingrong@uniontech.cn> - 9.56.1-7
- Type:CVE
- ID:NA
- SUG:NA
- DECS: This is the second part of the fix for CVE-2024-33869

* Thu Jul 04 2024 zhangxianting <zhangxianting@uniontech.com> - 9.56.1-6
- Type:CVE
- ID:NA
- SUG:NA
- DECS: fix CVE-2024-29506 CVE-2024-29507 CVE-2024-29508 CVE-2024-29509 CVE-2024-29511

* Fri May 10 2024 xuchenchen <xuchenchen@kylinos.cn> - 9.56.1-5
- Type:CVE
- ID:NA
- SUG:NA
- DECS: fix CVE-2024-29510 CVE-2024-33869 CVE-2024-33870 CVE-2024-33871

* Sun Apr 28 2024 xuchenchen <xuchenchen@kylinos.cn> - 9.56.1-4 
- Type:CVE
- ID:NA
- SUG:NA
- DECS: fix CVE-2023-52722

* Mon Dec 25 2023 liningjie <liningjie@xfusion.com> - 9.56.1-3
- Type:CVE
- ID:NA
- SUG:NA
- DESC:fix CVE-2023-46751

* Wed Aug  2 2023 dillon chen <dillon.chen@gmail.com> - 9.56.1-2
- Type:CVE
- ID:NA
- SUG:NA
- DESC:fix CVE-2023-28879 CVE-2023-36664 CVE-2023-38559

* Mon Jun 20 2022 dillon chen <dillon.chen@gmail.com> - 9.56.1-1
- update vserion to 9.56.1

* Wed Dec 29 2021 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 9.55.0-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update vserion to 9.55.0

* Mon Apr 19 2021 panxiaohe <panxiaohe@huawei.com> - 9.52-5
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:use make macros to run check in parallel

* Sat Oct 31 2020 Liquor <lirui130@huawei.com> - 9.52-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix problems detected by oss-fuzz test

* Thu Sep 10 2020 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 9.52-3
- Type:bugfix
- ID:CVE-2020-15900
- SUG:NA
- DESC:fix CVE-2020-15900

* Thu Sep 3 2020 wangchen <wangchen137@huawei.com> - 9.52-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Sync some patches from community

* Sun Jun 28 2020 wangchen <wangchen137@huawei.com> - 9.52-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update ghostscript to 9.52

* Wed Mar 18 2020 openEuler Buildteam <buildteam@openeuler.org> - 9.27-8
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix autosetup patch

* Wed Mar 18 2020 openEuler Buildteam <buildteam@openeuler.org> - 9.27-7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add run dvipdf securely

* Tue Jan 7 2020 chengquan<chengquan3@huawei.com> - 9.27-6
- Type:CVE
- ID:NA
- SUG:NA
- DESC:fix CVE-2019-14869

* Tue Jan 7 2020 chengquan<chengquan3@huawei.com> - 9.27-5
- Type:CVE
- ID:NA
- SUG:NA
- DESC:remove useless patch

* Fri Jan 3 2020 wangxiao<wangxiao65@huawei.com> - 9.27-4
- Type:CVE
- ID:NA
- SUG:NA
- DESC:fix CVE-2019-14811 CVE-2019-14812 CVE-2019-14813 CVE-2019-14817

* Mon Sep 23 2019 openEuler Buildteam <buildteam@openeuler.org> - 9.27-3
- fix CVE-2019-10216 and modify requires

* Mon Sep 23 2019 openEuler Buildteam <buildteam@openeuler.org> - 9.27-2
- Add subpackage tools-dvipdf

* Thu Sep 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 9.27-1
- Package init
